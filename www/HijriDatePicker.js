/**
 * Cordova HijriDatePicker Plugin Copyright (c) Ejada Free Licensed
 */

/**
 * Constructor
 */
function HijriDatePicker() {
  this._callback;
}

/**
 * show - show the picker
 */
HijriDatePicker.prototype.show = function(options, cb, errCb) {

	var defaults = {
		mode : 'h',
		date : '',
		minYear: 1300,
		maxYear: 1600,
		locale: 'en',
        doneButtonLabel: 'Done',
        doneButtonColor: '#007AFF',
        cancelButtonLabel: 'Cancel',
        cancelButtonColor: '#007AFF',		
        x: '0',
        y: '0',
        popoverArrowDirection: this._popoverArrowDirectionIntegerFromString("any"),	
        allowOldDates: true,
        allowFutureDates: true,
        minDate: '',
        maxDate: ''			
	};

	for (var key in defaults) {
		if (typeof options[key] !== "undefined") {
			defaults[key] = options[key];
		}
	}

	this._callback = cb;

	var callback = function(message) {
		if(message != 'error'){
			cb(message)
		} else {
			// TODO error popup?
    	}
	}

	var errCallback = function(message) {
		if (typeof errCb === 'function') {
			errCb(message);
		}
	}

	cordova.exec(callback,
		errCallback,
		"HijriDatePicker",
		"show",
		[defaults]
	);
};

HijriDatePicker.prototype._dateSelected = function(date) {
    //var d = new Date(parseFloat(date) * 1000);
	//alert(date);
    if (this._callback)
        this._callback(date);
};

HijriDatePicker.prototype._dateSelectionCanceled = function() {
    /*if (this._callback)
        this._callback();*/
};

HijriDatePicker.prototype._UIPopoverArrowDirection = {
    "up": 1,
    "down": 2,
    "left": 4,
    "right": 8,
    "any": 15
};

HijriDatePicker.prototype._popoverArrowDirectionIntegerFromString = function (string) {
    if (typeof this._UIPopoverArrowDirection[string] !== "undefined") {
        return this._UIPopoverArrowDirection[string];
    }
    return this._UIPopoverArrowDirection.any;
};

var hijriDatePicker = new HijriDatePicker();
module.exports = hijriDatePicker;

// Make plugin work under window.plugins
if (!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.hijriDatePicker) {
    window.plugins.hijriDatePicker = hijriDatePicker;
}