/**
 * @author Ejada
 * Cordova Hijri Date picker plugin
 */

package com.ejada.plugin.hijridatepicker;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import net.alhazmy13.hijridatepicker.HijriCalendarDialog;
import net.alhazmy13.hijridatepicker.HijriCalendarView.OnDateSetListener;


public class HijriDatePicker extends CordovaPlugin {

	private final String pluginName = "HijriDatepicker";

	private static final String HIJRI = "h";

	// On some devices, onDateSet or onTimeSet are being called twice
	private boolean called = false;
	private boolean canceled = false;

	@Override
	public boolean execute(final String action, final JSONArray data, final CallbackContext callbackContext) {
		Log.d(pluginName, "HijriDatePicker called with options: " + data);
		called = false;
		canceled = false;
		boolean result = false;

		this.show(data, callbackContext);
		result = true;

		return result;
	}

	public synchronized void show(final JSONArray data, final CallbackContext callbackContext) {
		HijriDatePicker hijriDatePicker = this;
		Context currentCtx = cordova.getActivity();
		Runnable runnable;
		JsonDate jsonDate = new JsonDate().fromJson(data);

		// Retrieve options
		JSONObject options = data.optJSONObject(0);

		HijriCalendarDialog.Language language = jsonDate.locale.equals("en") ? HijriCalendarDialog.Language.English : HijriCalendarDialog.Language.Arabic;
		HijriCalendarDialog.Mode mode = jsonDate.mode.equals("h") ? HijriCalendarDialog.Mode.Hijri : HijriCalendarDialog.Mode.Gregorian;

		final DateSetListener dateSetListener = new DateSetListener(hijriDatePicker, callbackContext, jsonDate);
		if(jsonDate.day != 0){
			new HijriCalendarDialog.Builder(currentCtx)
					.setOnDateSetListener(dateSetListener)
					.setUILanguage(language)
					.setMode(mode)
					.setDefaultHijriDate(jsonDate.day, jsonDate.month, jsonDate.year)
					.setMinHijriYear(jsonDate.minYear)
					.setMaxHijriYear(jsonDate.maxYear)
					.show();
		}
		else{
			new HijriCalendarDialog.Builder(currentCtx)
					.setOnDateSetListener(dateSetListener)
					.setUILanguage(language)
					.setMode(mode)
					.setMinHijriYear(jsonDate.minYear)
					.setMaxHijriYear(jsonDate.maxYear)
					.show();
		}

		//runnable = runnableDatePicker(datePickerPlugin, theme, currentCtx, callbackContext, jsonDate);

		//cordova.getActivity().runOnUiThread(runnable);
	}

	private final class JsonDate {

		private String mode = HIJRI;
		private int minYear = 0;
		private int maxYear = 0;
		private int month = 0;
		private int day = 0;
		private int year = 0;
		private String locale = "en";

		public JsonDate() {
			reset();
		}

		private void reset() {
			year = 0;
			month = 0;
			day = 0;
		}

		public JsonDate fromJson(JSONArray data) {

			try {
				JSONObject obj = data.getJSONObject(0);
				mode = isNotEmpty(obj, "mode") ? obj.getString("mode")
						: HIJRI;

				minYear = isNotEmpty(obj, "minYear") ? obj.getInt("minYear") : 1300;
				maxYear = isNotEmpty(obj, "maxYear") ? obj.getInt("maxYear") : 1600;

				locale = isNotEmpty(obj, "locale") ? obj.getString("locale")
						: "en";

				String optionDate = obj.getString("date");

				if(optionDate.equals("") || optionDate == null){

				}
				else {
					String[] datePart = optionDate.split("/");
					day = Integer.parseInt(datePart[0]);
					month = Integer.parseInt(datePart[1]) - 1;
					year = Integer.parseInt(datePart[2]);
				}
			} catch (JSONException e) {
				reset();
			}

			return this;
		}

		public boolean isNotEmpty(JSONObject object, String key)
				throws JSONException {
			return object.has(key)
					&& !object.isNull(key)
					&& object.get(key).toString().length() > 0
					&& !JSONObject.NULL.toString().equals(
					object.get(key).toString());
		}

	}

	private final class DateSetListener implements OnDateSetListener {
		private JsonDate jsonDate;
		private final HijriDatePicker hijriDatePicker;
		private final CallbackContext callbackContext;

		private DateSetListener(HijriDatePicker hijriDatePicker, CallbackContext callbackContext, JsonDate jsonDate) {
			this.hijriDatePicker = hijriDatePicker;
			this.callbackContext = callbackContext;
			this.jsonDate = jsonDate;
		}

		/**
		 * Return a string containing the date in the format YYYY/MM/DD 
		 */
		@Override
		public void onDateSet(final int year, final int monthOfYear, final int dayOfMonth) {
			if (canceled || called) {
				return;
			}
			called = true;
			canceled = false;

			Log.d("onDateSet", "called: " + called);
			Log.d("onDateSet", "canceled: " + canceled);
			Log.d("onDateSet", "mode: " + jsonDate.mode);


			String returnDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
			Log.d("onDateSet", "returnDate: " + returnDate);

			callbackContext.success(returnDate);
		}
	}
}