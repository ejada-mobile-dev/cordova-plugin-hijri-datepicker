/*
 Cordova HijriDatePicker Plugin
 
 Free Licensed
*/

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface HijriDatePicker : CDVPlugin <UIPopoverControllerDelegate> {
    
}

- (void)show:(CDVInvokedUrlCommand*)command;

@end